package main

import (
	"fmt"

	"sync"
)

func worker(ports chan int, wg*  sync.WaitGroup){

	for port := range ports {
		fmt.Println(port)
		wg.Done()
	}
}

func main(){

	//Make our channels four our ports 
	ports := make(chan int, 100)

	var wg sync.WaitGroup

	//Starts our workers who continously receive until the channel is closed
	for i := 0;  i < cap(ports);  i++{
		go worker(ports, &wg)
	}

	//Now we send a port to one of our workers
	for i:=1; i < 1024;  i++{
		wg.Add(1)
		ports<-i
	}

	//Wait for all of our work to be done
	wg.Wait()

	//Now close all ports
	close(ports)

}
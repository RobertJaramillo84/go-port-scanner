package main

import (
	"fmt"
	"net"
	"sort"
)

func worker(ports chan int, results chan int) {

	for port := range ports {

		//Set the address
		address := fmt.Sprintf("scanme.nmap.org:%d", port)

		conn, err := net.Dial("tcp", address)
		if nil != err {
			results <- 0
			continue
		}

		conn.Close()
		results<-port

	}
}

func main() {

	//Make our channels four our ports
	ports := make(chan int, 300)
	results := make(chan int)
	var openPorts []int
	fmt.Println("0")
	//Starts our workers who continously receive until the channel is closed
	for i := 0; i < cap(ports); i++ {
		go worker(ports, results)
	}
	go func() {
		//Now we send a port to one of our workers
		for i := 1; i < 1024; i++ {
			ports <-i
		}
	}()

    
	//Now collect our results and sort them
	for i := 1; i < 1024; i++ {
		port := <-results
		fmt.Printf("%d\r\n", i)
		if port != 0 {
			openPorts = append(openPorts, port)
		}
	}

	//Now close all ports
	close(ports)
	close(results)

	sort.Ints(openPorts)
	fmt.Printf("Open Ports\r\n____________\r\n")
	for _, port := range openPorts {
		fmt.Printf("%d\n", port)
	}

}
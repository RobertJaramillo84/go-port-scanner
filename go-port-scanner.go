package main

import (
	"fmt"
	"net"
)

func main() {

   for port:=1; port<1024; port++{
	    address := fmt.Sprintf("scanme.nmap.org:%d", port)
		conn, err := net.Dial("tcp", address)

		if nil == err{
			fmt.Printf("Connection Successful %d - address:%s\r\n", port. address)
		} else {
			fmt.Printf("Port closed %d - address:%s\r\n", port, address)
			continue
		}
		conn.Close()
	}
}
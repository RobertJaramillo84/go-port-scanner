package main

import (
	"fmt"
	"net"
	"sync"
)

func main() {

	var waitGroup sync.WaitGroup

	for port := 1; port < 10; port++ {
        waitGroup.Add(1)
		go func(port int) {
			defer waitGroup.Done()
			address := fmt.Sprintf("scanme.nmap.org:%d", port)
			conn, err := net.Dial("tcp", address)

			if nil != err {
				return
			}
			conn.Close()
			fmt.Printf("Port %d: OPEN\r\n", port)
		}(port)
	}
	waitGroup.Wait()
}
